# Demo for Colony
Create a simple Contacts Manager web application that lists, saves, adds and searches
Use React.js (with Flux or redux) for the frontend.
Express for the backend (rest API).
Use bootstrap and Google Material design for layout.
